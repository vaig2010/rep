<?php /* Smarty version Smarty-3.1.21, created on 2016-06-15 11:49:25
         compiled from "/var/www/html/cscart_i/design/backend/templates/addons/reward_points/hooks/orders/totals_content.post.tpl" */ ?>
<?php /*%%SmartyHeaderCode:743692371576116952c5677-54102119%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '537c6a79dca82b1fbc7389fe6a2e29008e5e44d0' => 
    array (
      0 => '/var/www/html/cscart_i/design/backend/templates/addons/reward_points/hooks/orders/totals_content.post.tpl',
      1 => 1462870358,
      2 => 'tygh',
    ),
  ),
  'nocache_hash' => '743692371576116952c5677-54102119',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'order_info' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.21',
  'unifunc' => 'content_576116952e7d40_12292955',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_576116952e7d40_12292955')) {function content_576116952e7d40_12292955($_smarty_tpl) {?><?php
fn_preload_lang_vars(array('points','points_lowercase','points_in_use','points_lowercase'));
?>
<?php if ($_smarty_tpl->tpl_vars['order_info']->value['points_info']['reward']) {?>
    <tr>
        <td><?php echo $_smarty_tpl->__("points");?>
:</td>
        <td><?php echo $_smarty_tpl->__("points_lowercase",array($_smarty_tpl->tpl_vars['order_info']->value['points_info']['reward']));?>
</td>
    </tr>
<?php }?>

<?php if ($_smarty_tpl->tpl_vars['order_info']->value['points_info']['in_use']) {?>
    <tr>
        <td><?php echo $_smarty_tpl->__("points_in_use");?>
&nbsp;(<?php echo $_smarty_tpl->__("points_lowercase",array($_smarty_tpl->tpl_vars['order_info']->value['points_info']['in_use']['points']));?>
):</td>
        <td><?php echo $_smarty_tpl->getSubTemplate ("common/price.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, null, array('value'=>$_smarty_tpl->tpl_vars['order_info']->value['points_info']['in_use']['cost']), 0);?>
</td>
    </tr>
<?php }?><?php }} ?>
